//
//  PlayAreaView.swift
//  N-back
//
//  Created by Emil on 12/07/16.
//  Copyright © 2016 Emil. All rights reserved.
//

import UIKit
import AVFoundation

class PlayAreaView: UIView {
    
    //    override func drawRect(rect: CGRect) {
    //        var center = CGPoint(x: bounds.midX, y: bounds.midY)
    //
    //        let skull = UIBezierPath(arcCenter: center, radius: 30.0, startAngle: 0.0, endAngle: CGFloat(2*M_PI), clockwise: true)
    //        skull.lineWidth = 5.0
    //        UIColor.blueColor().set()
    //        skull.stroke()
    //
    //
    //        var cell = CGRect(origin: CGPoint(x: bounds.midX, y: bounds.midY), size: CGSize(width: 50, height: 50))
    //
    //        let blob = UIBezierPath(roundedRect: cell, cornerRadius: 5.5)
    //        UIColor.blackColor().set()
    //        blob.stroke()
    //
    //    }
    
    var soundGame = false
    var positionGame = false
    var colorGame = false
    
    var cellWidth: CGFloat!
    var cellSpace: CGFloat!
    var cellDisapperDelayTime: CGFloat!
    var cellDisapperAnimationTime: CGFloat!
    
    var leftMargin: CGFloat!
    
    var currentTag = -1
    
    var ithIteration = 0 // to store ith iteration, to check
    var totalIterations = 0
    var level: Int!
    
    var positionStack = [Int]() // to store the tag values of the squares ie, to remember!
    var userPositionMatchScore = 0
    var userPositionMismatchScore = 0
    var actualPositionMatchScore = 0
   
    var soundStack = [String]()
    var userSoundMatchScore = 0
    var userSoundMismatchScore = 0
    var actualSoundMatchScore = 0
    
    var colorStack = [Int]()
    var userColorMatchScore = 0
    var userColorMismatchScore = 0
    var actualColorMatchScore = 0
    
    var tagOffset = 1000 // to offset view tag numbers
    
    var matchButtons: Array<UIButton> = []
    
    var alphabetSound: AVAudioPlayer!
    var soundPath: String!
    var soundUrl: NSURL!
    
    // For reusing the cell for drawing at another location
    var cell: UIView!
    var _frame: CGRect!
    
    private struct ratios {
        static let cellToWidthOfScreen = CGFloat(0.9) // 3 cells covers 90% of screen width
    }
    
    func setCellProperties() {
        cellWidth = 90
        cellSpace = 10
    }
    
    func setGameProperties(num: Int, animationTime: CGFloat, delayTime: CGFloat, iterations: Int, position: Bool,
                           sound: Bool, color: Bool) {
        level = num
        ithIteration = -1
        positionStack = []
        cellDisapperAnimationTime = animationTime
        cellDisapperDelayTime = delayTime
        totalIterations = iterations
        soundGame = sound
        positionGame = position
        colorGame = color
    }
    
    func getCellFrame(index: Int) -> CGRect {
        var frame: CGRect
        switch(index) {
        case 0: frame = CGRect(
            origin: CGPoint(x: bounds.midX - cellWidth/2 - cellSpace - cellWidth, y: bounds.midY - cellWidth/2 - cellSpace - cellWidth),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        case 1: frame = CGRect(
            origin: CGPoint(x: bounds.midX - cellWidth/2, y: bounds.midY - cellWidth/2 - cellSpace - cellWidth),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        case 2: frame = CGRect(
            origin: CGPoint(x: bounds.midX + cellWidth/2 + cellSpace, y: bounds.midY - cellWidth/2 - cellSpace - cellWidth),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        case 3: frame = CGRect(
            origin: CGPoint(x: bounds.midX - cellWidth/2 - cellSpace - cellWidth, y: bounds.midY - cellWidth/2),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        case 4: frame = CGRect(
            origin: CGPoint(x: bounds.midX - cellWidth/2, y: bounds.midY - cellWidth/2),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        case 5: frame = CGRect(
            origin: CGPoint(x: bounds.midX + cellWidth/2 + cellSpace, y: bounds.midY - cellWidth/2),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        case 6: frame = CGRect(
            origin: CGPoint(x: bounds.midX - cellWidth/2 - cellSpace - cellWidth, y: bounds.midY + cellWidth/2 + cellSpace),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        case 7:frame = CGRect(
            origin: CGPoint(x: bounds.midX - cellWidth/2, y: bounds.midY + cellWidth/2 + cellSpace),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        case 8: frame = CGRect(
            origin: CGPoint(x: bounds.midX + cellWidth/2 + cellSpace, y: bounds.midY + cellWidth/2 + cellSpace),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        default: frame = CGRect(
            origin: CGPoint(x: bounds.midX + cellWidth/2 + cellSpace, y: bounds.midY + cellWidth/2 + cellSpace),
            size: CGSize(width: cellWidth, height: cellWidth)
            )
        }
        
        return frame
    }
    
    func getColor(i: Int) -> UIColor {
        var color: UIColor!
        
        switch (i) {
        case 0: color = UIColor.blackColor()
        case 1: color = UIColor.yellowColor()
        case 2: color = UIColor.brownColor()
        case 3: color = UIColor.redColor()
        case 4: color = UIColor.orangeColor()
        case 5: color = UIColor.magentaColor()
        case 6: color = UIColor.blueColor()
        case 7: color = UIColor.cyanColor()
        case 8: color = UIColor.purpleColor()
        default: color = UIColor.whiteColor()
        }
        return color
    }
    
    func getSound(i: Int) -> String {
        switch(i) {
        case 1: return "a"
        case 2: return "b"
        case 3: return "c"
        case 4: return "d"
        case 5: return "e"
        case 6: return "f"
        case 7: return "g"
        case 8: return "h"
        case 9: return "i"
        case 10: return "j"
        case 11: return "k"
        case 12: return "l"
        case 13: return "m"
        case 14: return "n"
        case 15: return "o"
        case 16: return "p"
        case 17: return "q"
        case 18: return "r"
        case 19: return "s"
        case 20: return "t"
        case 11: return "u"
        case 22: return "v"
        case 23: return "w"
        case 24: return "x"
        case 25: return "y"
        case 26: return "z"
        default: return "a"
        }
    }
    
    func addCell() {
        /*
         Strange behaviour here.
         Let cell.tag = 0 (due to randomness) and later removedFromSuperView(),
         In the next iteration of nextTic the file entire playAreaView was
         unwrapped to nil in the ViewController. Took a long time to figure out
         this was the issue. However, did not try to understand why this was
         happening. Just observed the tag numbers and found 0 was causing problem, 
         removed 0 from becoming the tag of a cell.
         
         Problem Hint: Tags are automatically assigned to views by Swift on its own,
         and I guess tag number 0 is given to the playAreaView when it is created. So, when 
         you pop it out you lost the reference to it therefore gets it unwrapped to nil.
         One solution is to offset (tagOffset) the view from a number eg: 1000 + 1, 1000 + 2 etc
         */
        ithIteration += 1
        

        let defaultPosition = 4
        _frame = getCellFrame(defaultPosition)
        var color = getColor(0)
        var loc = 0
        currentTag = tagOffset
        
        if positionGame {
            loc = Int(arc4random_uniform(9)) + 1
            currentTag = tagOffset + loc
            _frame = getCellFrame(loc - 1)
            currentTag = tagOffset + loc
            positionStack.append(loc)
        }
        
        if colorGame {
            loc = Int(arc4random_uniform(9))
            color = getColor(loc)
            //TODO - there is no color stack
            colorStack.append(loc)
        }
        
        if soundGame {
            loc = Int(arc4random_uniform(25)) + 1
            let soundLoc = getSound(loc)
            soundPath = NSBundle.mainBundle().pathForResource(
                soundLoc,
                ofType: "mp3"
            )
            soundUrl = NSURL(fileURLWithPath: soundPath!)
            do {
                alphabetSound = try AVAudioPlayer(contentsOfURL: soundUrl)
                alphabetSound.volume = 1.0
                alphabetSound.play()
                soundStack.append(soundLoc)
            } catch {
                print("Some error happened while playing sound")
            }
            
            if !positionGame && !colorGame {
                color = UIColor.greenColor()
            }
        }
        
        cell = UIView(frame: _frame)
        cell.backgroundColor = color
        cell.tag = currentTag
        addSubview(cell)
        
        if ithIteration >= level {
            enableMatchButtons()
        }
        
        if (positionGame) {
            if (ithIteration >= level) {
                if positionStack[ithIteration] == positionStack[ithIteration - level] {
                    actualPositionMatchScore += 1
                }
            }
        }
        
        if (soundGame) {
            if (ithIteration >= level) {
                if soundStack[ithIteration] == soundStack[ithIteration - level] {
                    actualSoundMatchScore += 1
                }
            }
        }
        

        removeCell()
    }
    
    func removeCell() {
        let viewWithTag = self.viewWithTag(currentTag)
        UIView.animateWithDuration(
            Double(cellDisapperAnimationTime),
            delay: Double(cellDisapperDelayTime),
            options: [UIViewAnimationOptions.CurveLinear],
            animations: {
                viewWithTag?.alpha = 0.0
            },
            completion: {
                if $0 {
                    viewWithTag?.removeFromSuperview()
                }
            }
        )
    }
    
    func drawMatchButtons(sound: Bool, position: Bool, color: Bool) {
        var noOfButtons = 0
        //could be simplified through k-map to find only two required buttons https://www.facstaff.bucknell.edu/mastascu/eLessonsHTML/Logic/Logic3.html
        // and  http://stackoverflow.com/questions/3076078/check-if-at-least-two-out-of-three-booleans-are-true/3076173#3076173
//        if (sound && (position || color) || (position && color)) {
        if (position && sound && !color || position && !sound && color || !position && sound && color ) {
            noOfButtons = 2
        } else if (sound && position && color) {
            noOfButtons = 3
        } else {
            noOfButtons = 1
        }
        
        if noOfButtons == 1 {
            let buttonOriginX = bounds.midX - cellWidth/2
            let buttonOriginY = bounds.midY - cellWidth/2 + 2.5 * cellWidth
            if sound {
                drawSoundMatchButton(buttonOriginX, buttonOriginY: buttonOriginY)
            } else if position {
                drawPositionMatchButton(buttonOriginX, buttonOriginY: buttonOriginY)
            } else if color {
                drawColorMatchButton(buttonOriginX, buttonOriginY: buttonOriginY)
            } else {
                print("something went wrong with identifying the number of active buttons 1")
            }
        } else if noOfButtons == 2 {
            let buttonOriginX1 = bounds.midX - cellWidth/2 - 1 * cellWidth
            let buttonOriginY1 =  bounds.midY - cellWidth/2 + 2.5 * cellWidth
            let buttonOriginX2 = bounds.midX - cellWidth/2 + 1 * cellWidth
            let buttonOriginY2 = bounds.midY - cellWidth/2 + 2.5 * cellWidth
            
            if position && sound {
                drawPositionMatchButton(buttonOriginX1, buttonOriginY: buttonOriginY1)

                drawSoundMatchButton(buttonOriginX2, buttonOriginY: buttonOriginY2)
            } else if position && color {
                //position left, color right
                drawPositionMatchButton(buttonOriginX1, buttonOriginY: buttonOriginY1)
                drawColorMatchButton(buttonOriginX2, buttonOriginY: buttonOriginY2)
            } else if color && sound {
                // color left, sound right
                drawColorMatchButton(buttonOriginX1, buttonOriginY: buttonOriginY1)
                drawSoundMatchButton(buttonOriginX2, buttonOriginY: buttonOriginY2)
            } else {
                print("something went wrong with identifying the number of active buttons 2")
            }
        } else if noOfButtons == 3 {
            let buttonOriginX1 = bounds.midX - cellWidth/2 - 1.2 * cellWidth
            let buttonOriginY1 =  bounds.midY - cellWidth/2 + 2.5 * cellWidth
            let buttonOriginX2 = bounds.midX - cellWidth/2 + 1.2 * cellWidth
            let buttonOriginY2 = bounds.midY - cellWidth/2 + 2.5 * cellWidth
            let buttonOriginX3 = bounds.midX - cellWidth/2 + 0 * cellWidth
            let buttonOriginY3 = bounds.midY - cellWidth/2 + 2.5 * cellWidth
            drawPositionMatchButton(buttonOriginX1, buttonOriginY: buttonOriginY1)
            drawSoundMatchButton(buttonOriginX2, buttonOriginY: buttonOriginY2)
            drawColorMatchButton(buttonOriginX3, buttonOriginY: buttonOriginY3)
            print("here")
        }
        

    }
    
    func drawPositionMatchButton(buttonOriginX: CGFloat, buttonOriginY: CGFloat) -> UIButton {
        let buttonWidth = cellWidth
        let positionMatchButton = UIButton(type: .Custom)
        positionMatchButton.frame = CGRectMake(
            buttonOriginX,
            buttonOriginY,
            buttonWidth,
            buttonWidth
        )
        positionMatchButton.layer.cornerRadius = buttonWidth/2
        positionMatchButton.backgroundColor = UIColor.grayColor()
        positionMatchButton.setTitle("Position", forState: .Normal)
        positionMatchButton.addTarget(
            self,
            action: #selector(positionButtonPressed),
            forControlEvents: .TouchUpInside
        )
        addSubview(positionMatchButton)
        matchButtons.append(positionMatchButton)
        return positionMatchButton
    }
    
    func drawSoundMatchButton(buttonOriginX: CGFloat, buttonOriginY: CGFloat) -> UIButton {
        let buttonWidth = cellWidth
        let soundMatchButton = UIButton(type: .Custom)
        soundMatchButton.frame = CGRectMake(
            buttonOriginX,
            buttonOriginY,
            buttonWidth,
            buttonWidth
        )
        soundMatchButton.layer.cornerRadius = buttonWidth/2
        soundMatchButton.backgroundColor = UIColor.greenColor()
        soundMatchButton.setTitle("Sound", forState: .Normal)
        soundMatchButton.addTarget(
            self,
            action: #selector(soundButtonPressed),
            forControlEvents: .TouchUpInside
        )
        addSubview(soundMatchButton)
        matchButtons.append(soundMatchButton)
        return soundMatchButton
    }
    
    func drawColorMatchButton(buttonOriginX: CGFloat, buttonOriginY: CGFloat) -> UIButton {
        let buttonWidth = cellWidth
        let colorMatchButton = UIButton(type: .Custom)
        colorMatchButton.frame = CGRectMake(
            buttonOriginX,
            buttonOriginY,
            buttonWidth,
            buttonWidth
        )
        colorMatchButton.layer.cornerRadius = buttonWidth/2
        colorMatchButton.backgroundColor = UIColor.magentaColor()
        colorMatchButton.setTitle("Color", forState: .Normal)
        colorMatchButton.addTarget(
            self,
            action: #selector(colorButtonPressed),
            forControlEvents: .TouchUpInside
        )
        addSubview(colorMatchButton)
        matchButtons.append(colorMatchButton)
        return colorMatchButton
    }
    
    func disableMatchButtons() {
        for i in 0..<matchButtons.count {
            let button = matchButtons[i]
            button.enabled = false
            button.alpha = 0.2
        }
    }
    
    func disableOneMatchButton(sender: UIButton) {
        sender.enabled = false
        sender.alpha = 0.2
    }
    
    func enableMatchButtons() {
        for i in 0..<matchButtons.count {
            let button = matchButtons[i]
            button.enabled = true
            button.alpha = 1.0
        }
    }
    
    func positionButtonPressed(sender: UIButton) {
        if positionStack[ithIteration] == positionStack[ithIteration - level] {
            userPositionMatchScore += 1
        } else {
            userPositionMismatchScore += 1
        }
        disableOneMatchButton(sender)
    }
    
    func soundButtonPressed(sender: UIButton) {
        if soundStack[ithIteration] == soundStack[ithIteration - level] {
            userSoundMatchScore += 1
        } else {
            userSoundMismatchScore += 1
        }
        disableOneMatchButton(sender)
    }
    
    func colorButtonPressed(sender: UIButton) {
        if colorStack[ithIteration] == colorStack[ithIteration - level] {
            userColorMatchScore += 1
        } else {
            userColorMismatchScore += 1
        }
        disableOneMatchButton(sender)
    }
    
    func getScore() {
        
        var totalScorePercentage = [Double]()
        
        if soundGame {
            let mismatch = Double(actualSoundMatchScore) - Double(userSoundMatchScore) + Double(userSoundMismatchScore)
            let soundScorePercentage = (Double(totalIterations) - Double(mismatch)) / Double(totalIterations)
       
            print("sound result", soundStack, userSoundMatchScore, userSoundMismatchScore, actualSoundMatchScore, soundScorePercentage)
            totalScorePercentage.append(soundScorePercentage)
        }
        
        if positionGame {
            let mismatch = Double(actualPositionMatchScore) - Double(userPositionMatchScore) + Double(userPositionMismatchScore)
            let positionScorePercentage = (Double(totalIterations) - Double(mismatch) ) / Double(totalIterations)
            
            print("position result", positionStack, userPositionMatchScore, userPositionMismatchScore, actualPositionMatchScore, positionScorePercentage)
            totalScorePercentage.append(positionScorePercentage)
        }
        
        if colorGame {
            let mismatch = Double(actualColorMatchScore) - Double(userColorMatchScore) + Double(userColorMismatchScore)
            let colorScorePercentage = (Double(totalIterations) - Double(mismatch) ) / Double(totalIterations)
            
            print("color result", colorStack, userColorMatchScore, userColorMismatchScore, actualColorMatchScore, colorScorePercentage)
            totalScorePercentage.append(colorScorePercentage)
        }
        print("final", totalScorePercentage)
    }
}
