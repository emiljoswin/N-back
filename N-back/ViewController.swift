//
//  ViewController.swift
//  N-back
//
//  Created by Emil on 12/07/16.
//  Copyright © 2016 Emil. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var playAreaView: PlayAreaView!
    
    var noOfTimes: Int!
    var timerTimes: Int = 0
    var timer: NSTimer!
    var sound: AVAudioPlayer!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        // The bounds's values become correct from the outlet only in this function.
        // It wasn't correct when called from viewWillAppear()
        let level = 1
        let iterations = 10
        let cellDisapperAnimationTime = 0.2
        let cellDisapperDelayTime = 0.4
        
        let soundGame = true
        let positionGame = true
        let colorGame = true
        
        playAreaView.setGameProperties(
            level,
            animationTime: CGFloat(cellDisapperAnimationTime),
            delayTime: CGFloat(cellDisapperDelayTime),
            iterations: iterations,
            position: positionGame,
            sound: soundGame,
            color: colorGame
        )
        
        playAreaView.setCellProperties()
        playAreaView.drawMatchButtons(soundGame, position: positionGame, color: colorGame)
//        playAreaView.drawPositionMatchButton()
//        playAreaView.drawSoundMatchButton()
        
        playAreaView.disableMatchButtons()
        
        self.noOfTimes = iterations + 1
        startTimer()
    }

    override func shouldAutorotate() -> Bool {
        return false
    }

    func startTimer() {
        
        timer = NSTimer.scheduledTimerWithTimeInterval(
            2.0,
            target: self,
            selector: #selector(nextTic),
            userInfo: nil,
            repeats: false
        )
    }
    
    func nextTic() {
        if timerTimes < noOfTimes {
            timerTimes += 1
            playAreaView.addCell()
            startTimer()
        } else {
            timer.invalidate()
            playAreaView.getScore()
        }
    }
}

